<?php

namespace AppBundle\Service;

use AppBundle\Exception\UnsupportedOperationException;

/**
 * Class Calculator
 * @package AppBundle\Service
 */
class Calculator
{
    const OPERATION_PLUS = 'plus';
    const OPERATION_MINUS = 'minus';

    /**
     * @param int $a
     * @param int $b
     * @param string $operation [plus, minus]
     * @return int
     * @throws UnsupportedOperationException
     */
    public function calculate($a, $b, $operation)
    {
        switch ($operation) {
            case self::OPERATION_PLUS:
                return $this->plus($a, $b);
                break;
            case self::OPERATION_MINUS:
                return $this->minus($a, $b);
                break;
            default:
                throw new UnsupportedOperationException("Operation {$operation} is not supported");
        }
    }

    /**
     * @param int $a
     * @param int $b
     * @return int
     */
    private function plus($a, $b)
    {
        return $a + $b;
    }

    /**
     * @param int $a
     * @param int $b
     * @return int
     */
    private function minus($a, $b)
    {
        return $a - $b;
    }
}