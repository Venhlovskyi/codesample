<?php

namespace AppBundle\Controller;

use AppBundle\Service\Calculator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CalculatorController extends Controller
{
    /**
     * @Route("/", name="calculate_page")
     */
    public function calculateAction(Request $request)
    {
        $a = (int) $request->get('a');
        $b = (int) $request->get('b');
        $operation = $request->get('operation');

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'result' => $this->get(Calculator::class)->calculate($a, $b, $operation)
        ]);
    }
}
